﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.Models;

namespace Company.Models
{
    public class MyDbContext:DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext>options):base(options)
        {

        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            //base.OnModelCreating(builder);
            //builder.Entity<Employee>().HasData(
           //new Employee() { EmployeeId = 1, FullName = "Ann", NIC = "123456456963" },
           //new Employee() { EmployeeId = 2, FullName = "Sam", NIC = "123456456123" },
         //  new Employee() { EmployeeId = 3, FullName = "John", NIC = "123456456789" });
        }
        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();
            return base.SaveChanges();
        }
        public DbSet<Company.Models.Department> Department { get; set; }
        public DbSet<Company.Models.Employee> Employee { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Company.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }

        [Column("FullName", TypeName = "varchar")]
        [StringLength(100)]
        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [Column("NIC", TypeName = "varchar")]
        [StringLength(12)]
        [Required]
        [DisplayName("NIC")]
        public string NIC { get; set; }

        [NotMapped]
        [DisplayName("Upload File")]
        public IFormFile ImageFile { get; set; }

        public string CurrentDeptCode { get; set; }
        public Department CurrentDepartment { get; set; }
    }
}
